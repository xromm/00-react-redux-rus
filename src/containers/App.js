import React, { Component } from 'react'
import { connect } from 'react-redux'

import Page from '../components/Page'
import User from '../components/User'

class App extends Component {
  render() {

    const { user, page } = this.props

    var numberOfPhotos = page.photos.length

    return <div>
        <p> Привет из Appddd, { user.name } { user.surename } ! </p>
        <p> Are your age is about { user.age }? </p>
        <p> year: { page.year }, photos for year = { numberOfPhotos } </p>
      </div>
  }
}

function mapStateToProps (state) {
  return {
    user: state.user,
    page: state.page
  }
}

export default connect(mapStateToProps)(App)
